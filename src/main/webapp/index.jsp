<%-- 
    Document   : index
    Created on : Apr 26, 2021, 12:26:30 AM
    Author     : const
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>

        <h1>Registro de Libros</h1>
        <form  name="form" action="LibroController" method="POST">
            <h2>Revisar lista de libros</h2>
            <button type="submit" name="accion" value="verlista" class="btn btn-success">Ver lista</button><br/> 
            
            <h2>Ingresar libro</h2>
            Isbn: <input type="text" name="isbn" id="isbn" > <br/> 
            Título: <input type="text" name="titulo" id="titulo" > <br/>   
            Autor: <input type="text" name="autor" id="autor" > <br/> 
            Genero: <input type="text" name="genero" id="genero" > <br/>            
            Editorial:<input type="text" name="editorial" id="editorial" > <br/>  
            Idioma: <input type="text" name="idioma" id="idioma" > <br/>             
            <button type="submit" name="accion" value="ingreso" class="btn btn-success">Ingresar</button>
           
        </form>

    </body>
</html>
