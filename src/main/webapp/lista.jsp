<%-- 
    Document   : lista
    Created on : Apr 26, 2021, 12:59:07 AM
    Author     : const
--%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="cl.entity.Libros"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    List<Libros> libros= (List<Libros>)request.getAttribute("listaLibros");
    Iterator<Libros> itlibros = libros.iterator();
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>lista</title>
    </head>
    <body>
        
        <form  name="form" action="LibroController" method="POST">

       
        <table border="1">
                    <thead>
                    <th>ISBN</th>
                    <th>Título </th>
                    <th>Autor</th>
                    <th>Género </th>                    
                    <th>Editorial</th>       
                    <th>Idioma</th>                     
                    <th> </th>
         
                    </thead>
                    <tbody>
                        <%while (itlibros.hasNext()) {
                       Libros lib = itlibros.next();%>
                        <tr>
                            <td><%= lib.getIsbn()%></td>
                            <td><%= lib.getTitulo()%></td>
                            <td><%= lib.getAutor()%></td>
                            <td><%= lib.getGenero()%></td>                           
                            <td><%= lib.getEditorial()%></td>
                            <td><%= lib.getIdioma()%></td>                            
                         
                 <td> <input type="radio" name="seleccion" value="<%= lib.getIsbn()%>"> </td>
                
                        </tr>
                        <%}%>                
                    </tbody>           
                </table>
            <button type="submit" name="accion" value="eliminar" class="btn btn-success">Eliminar</button>
            <button type="submit" name="accion" value="consultar" class="btn btn-success">Consultar</button>       
         </form>            
    </body>
</html>
