/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import cl.dao.LibrosJpaController;
import cl.dao.exceptions.NonexistentEntityException;
import cl.entity.Libros;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author const
 */
@WebServlet(name = "LibroController", urlPatterns = {"/LibroController"})
public class LibroController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet LibroController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet LibroController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        System.out.println("doPost");
        String accion = request.getParameter("accion");
        if (accion.equals("ingreso")) {
            try {
                String isbn = request.getParameter("isbn");
                String titulo = request.getParameter("titulo");
                String autor = request.getParameter("autor");
                String genero = request.getParameter("genero");
                String editorial = request.getParameter("editorial");
                String idioma = request.getParameter("idioma");

                Libros libros = new Libros();
                libros.setIsbn(isbn);
                libros.setTitulo(titulo);
                libros.setAutor(autor);
                libros.setGenero(genero);
                libros.setEditorial(editorial);
                libros.setIdioma(idioma);

                LibrosJpaController dao = new LibrosJpaController();
                dao.create(libros);

                processRequest(request, response);
            } catch (Exception ex) {
                Logger.getLogger(LibroController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        if (accion.equals("verlista")) {

            LibrosJpaController dao = new LibrosJpaController();
            List<Libros> lista = dao.findLibrosEntities();
            request.setAttribute("listaLibros", lista);
            request.getRequestDispatcher("lista.jsp").forward(request, response);

        }

        if (accion.equals("eliminar")) {

            try {
                String isbn = request.getParameter("seleccion");
                LibrosJpaController dao = new LibrosJpaController();
                dao.destroy(isbn);
            } catch (NonexistentEntityException ex) {
                Logger.getLogger(LibroController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (accion.equals("consultar")) {
            String isbn = request.getParameter("seleccion");
            LibrosJpaController dao = new LibrosJpaController();
            Libros registro = dao.findLibros(isbn);
            request.setAttribute("ver", registro);
            request.getRequestDispatcher("edicion.jsp").forward(request, response);

        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
